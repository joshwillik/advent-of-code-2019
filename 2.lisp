(ql:quickload :split-sequence)

(defun opcode-sum (state args)
       (destructuring-bind (n1 n2 address)
                           (subseq args 0 3)
         (setf (nth address state) (+ (nth n1 state) (nth n2 state)))
         state))

(defun opcode-multiply (state args)
       (destructuring-bind (n1 n2 address)
                           (subseq args 0 3)
         (setf (nth address state) (* (nth n1 state) (nth n2 state)))
         state))

(defun run-op (state ops)
       (destructuring-bind (op . rest)
                           ops
                           (if (= op 99)
                             (values state :final)
                             (values
                              (case op
                                (1 (opcode-sum state rest))
                                (2 (opcode-multiply state rest)))
                              :step))))

(defun run-ops (program)
  (let (hook (position 0))
    (loop
      (format t "~d ~A~%" position program)
      (multiple-value-setq (program hook)
                           (run-op program (subseq program position nil)))
      (setf position (+ position 4))
      (if (or (> position (list-length program)) (eq hook :final))
        (return))))
    program)

(defun parse-ops (text)
       (map 'list #'parse-integer (split-sequence:split-sequence #\, text)))

(defun read-file (name)
       (with-open-file (f name)
                       (let ((data (make-string (file-length f))))
                         (read-sequence data f)
                         data)))

(defun main ()
  (let* ((input (read-file "2.in.txt"))
        (program (parse-ops input)))
    (setf (nth 1 program) 12)
    (setf (nth 2 program) 2)
    (run-ops program)))
