(defun each-line (name fn)
	(with-open-file (f name)
		(loop for line = (read-line f nil nil)
			while line
    	do (funcall fn (parse-integer line)))))

(defun fuel-need (mass)
	(cond
		((< mass 3) 0)
		(t (let ((fuel (- (floor (/ mass 3)) 2)))
	   		(max 0 (+ fuel (fuel-need fuel)))))))

(let ((total-fuel 0))
	(each-line "1.in.txt" (lambda (mass)
		(let ((fuel (fuel-need mass)))
			(format t "Component ~d needs ~d fuel~%" mass fuel)
			(setf total-fuel (+ total-fuel fuel)))))
 	(format t "Total fuel needed: ~d~%" total-fuel))
